from django.shortcuts import render
from django.views import View
from django.views.generic.list import ListView
from quiz.models import Question, Option, Test
# Create your views here.
class TestList(View):
    
    def get(self, request):
        test = Test.objects.all()
        return render(request, "ui/test.html", {"object_list": test})


class TestDetail(View):

    def get(self, request, pk):
        test= Test.objects.get(pk=pk)
        questions_list= test.questions.all()
        return render(request,"ui/questionlist.html", {"test":test,"questions":questions_list})

class AddQuestion(View):

    def get(self,request,pk):
        question_pk= request.GET.get('qpk',None)
        # if question_pk is None:
        return render(request,'ui/question.html', {"test_pk":pk})
        # else:
