from django.shortcuts import render
from rest_framework import viewsets
from .serializers import QuestionSerializer, OptionSerializer, TestSerializer
from .models import Question, Option, Test
# Create your views here.

class QuestionViewset(viewsets.ModelViewSet):
    serializer_class= QuestionSerializer
    queryset= Question.objects.all()

class OptionViewset(viewsets.ModelViewSet):
    serializer_class= OptionSerializer
    queryset= Option.objects.all()

class TestViewSet(viewsets.ModelViewSet):
    serializer_class= TestSerializer
    queryset= Test.objects.all()
