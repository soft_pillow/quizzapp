from __future__ import unicode_literals
from __future__ import division
from django.db import models

# Create your models here.
class Test(models.Model):
    title = models.CharField(max_length=100)
    question_limit = models.PositiveIntegerField(default=0)
    created= models.DateTimeField(auto_now_add=True)
    updated= models.DateTimeField(auto_now=True)

    @property
    def question_count(self):
        return self.questions.count()

    @property
    def remaining_count(self):
        return (self.question_count/self.question_limit)*100

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('-created',)

class Question(models.Model):
    question= models.CharField(max_length=500)
    image= models.URLField(max_length=500, blank=True,verbose_name="Image URL")
    created= models.DateTimeField(auto_now_add=True)
    test = models.ForeignKey(Test, related_name="questions")

    def __unicode__(self):
        return self.question

    class Meta:
        ordering = ('-created',)

class Option(models.Model):
    option= models.CharField(max_length=500)
    image= models.URLField(max_length=500, blank=True,verbose_name="Image URL")
    question= models.ForeignKey(Question, related_name="options")

    def __unicode__(self):
        return self.option
